<?php

namespace Drupal\custom_taxe\OrderProcessor;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;

/**
 * Class CustomOrdersProcessor.
 */
class CustomOrdersProcessor implements OrderProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {

    try {
      // Get the current language.
      $language_id = \Drupal::languageManager()->getCurrentLanguage()->getId();
      // Get the store realted for this  current language.
      $ids = \Drupal::service('entity.query')
        ->get('commerce_store')
        ->condition('langcode', $language_id, '=')
        ->execute();
      if (count($ids) > 0) {
        $entitytype_manager = \Drupal::service('entity_type.manager');
        $storage = $entitytype_manager->getStorage('commerce_store');
        // Load all line item of options related to main product.
        $stores = $storage->loadMultiple($ids);
        $store = reset($ids);
        // Set the id of store.
        $order->setStoreId($store);
      }

    }
    catch (\Exception $e) {

      \Drupal::logger('custom_taxe')->error($e->getMessage());
    }
  }

}
