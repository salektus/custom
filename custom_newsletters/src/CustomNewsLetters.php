<?php

namespace Drupal\custom_newsletters;

use Drupal\sendinblue\SendinblueManager;
use Sendinblue\Mailin;

/**
 * Class CustomNewsLetters.
 */
class CustomNewsLetters {

  /**
   * Constructs a new CustomNewsLetters object.
   */
  public function __construct() {

  }

  /**
   * Get the list id of newsletter.
   *
   * @return array
   *   list of id.
   */
  private function getListId() {
    $id = [];
    $listid = SendinblueManager::getLists();
    foreach ($listid as $idlist) {

      $id[] = $idlist['id'];
    }

    return $id;
  }

  /**
   * Notify sendigblue for updating the html of newsletter.
   *
   * @param object $node
   *   The content type neswletter.
   */
  public function updateHtml($node) {

    $mailin = new Mailin("https://api.sendinblue.com/v2.0", SendinblueManager::getAccessKey());
    $data = [
      "from_name" => "[DEFAULT_FROM_NAME]",
      "from_email" => "*****@*****.fr",
      "name" => $node->title->value,
      "bat" => "",
      "html_url" => $node->toUrl()->setAbsolute()->toString(),
      "listid" => $this->getListId(),
      "subject" => $node->title->value,
      "reply_to" => "[DEFAULT_REPLY_TO]",
      "to_field" => "[PRENOM] [NOM]",
      'exclude_list' => [],
      "attachment_url" => "",
      "inline_image" => 1,
      "mirror_active" => 0,
      "send_now" => 0,

    ];
    // Create compaign when the newsletter is new.
    if (!$node->field_id_newsletters->value) {
      $responce = $mailin->create_campaign($data);
    }
    else {
      // Update the newsletter.
      $data['id'] = $node->field_id_newsletters->value;
      $responce = $mailin->update_campaign($data);
    }

    if ($responce['code'] === 'success') {
      // Save the id compagn .
      if (!$node->field_id_newsletters->value) {
        $node->set('field_id_newsletters', $responce['data']['id']);
        $node->save();
      }
    }
    else {
      \Drupal::messenger()->addMessage($responce['message'], 'error');
    }

  }

}
