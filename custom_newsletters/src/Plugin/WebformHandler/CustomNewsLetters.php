<?php

namespace Drupal\custom_newsletters\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\sendinblue\SendinblueManager;

/**
 * Form submission handler.
 *
 * Redirects to the [...] after the submit.
 *
 * @WebformHandler(
 *   id = "custom_newsletters",
 *   label = @Translation("Custom newsletter"),
 *   category = @Translation("Webform Handler"),
 *   description = @Translation("Suscribe newsletters"),
 *   cardinality =
 *       \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results =
 *    \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CustomNewsLetters extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'email' => '',
      'signup' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => t('Nom du champs email pour le newsletters'),
      '#default_value' => $this->configuration['email'],
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
    ];

    $ids = \Drupal::entityQuery(SENDINBLUE_SIGNUP_ENTITY)->execute();

    $options = [];

    if (count($ids) > 0) {

      $entity_manager = \Drupal::entityTypeManager();

      $signups = $entity_manager->getStorage(SENDINBLUE_SIGNUP_ENTITY)
        ->loadMultiple($ids);

      foreach ($signups as $signup) {
        $options[$signup->id()] = $signup->name->value;
      }

    }
    $form['signup'] = [
      '#type' => 'select',
      '#title' => t('Ralted form'),
      '#options' => $options,
      '#default_value' => $this->configuration['signup'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['email'] = $form_state->getValue('email');
  }

  /**
   * {@inheritdoc}
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    $values = $webform_submission->getData();
    $emailfield = $this->configuration['email'];
    $emailvalue = '';
    if (!empty($values[$emailfield])) {
      $emailvalue = $values[$emailfield];
    }

    $entity_manager = \Drupal::entityTypeManager();
    $signup = $entity_manager->getStorage(SENDINBLUE_SIGNUP_ENTITY)
      ->load($this->configuration['signup']);

    if (!$signup) {
      return;
    }
    $settings = (!$signup->settings->first()) ? [] : $signup->settings->first()
      ->getValue();
    $email_validator = \Drupal::service('email.validator');

    $email = $emailvalue;
    $list_id = $settings['subscription']['settings']['list'];

    if (!$email_validator->isValid($email)) {
      \Drupal::messenger()->addMessage(t('Invalid Email.'), 'error');
      return;
    }

    $response = SendinblueManager::validationEmail($email, $list_id);
    if ($response['code'] == 'invalid') {
      \Drupal::messenger()->addMessage(t('Invalid Email.'), 'error');
      return;
    }
    if ($response['code'] == 'already_exist') {
      \Drupal::messenger()->addMessage(t('You are already registered.'), 'warning');
      return;
    }

    $email_confirmation = $settings['subscription']['settings']['email_confirmation'];
    if ($email_confirmation == '1') {
      $templage_id = $settings['subscription']['settings']['template'];
    }

    $list_ids = $response['listid'];
    array_push($list_ids, $list_id);

    $info = [];
    $attributes = SendinblueManager::getAttributeLists();

    foreach ($attributes as $attribute) {
      $field_attribute_name = $form_state->getValue([
        'fields',
        $attribute['name'],
      ]);
      if (isset($field_attribute_name)) {
        $info[$attribute['name']] = $form_state->getValue([
          'fields',
          $attribute['name'],
        ]);
      }
    }
    $response_code = SendinblueManager::subscribeUser($email, $info, $list_ids);
    if ($response_code != 'success') {
      return;
    }

    // Store db.
    $data = SendinblueManager::getSubscriberByEmail($email);
    if ($data == FALSE) {
      $uniqid = uniqid();
      $data = [
        'email' => $email,
        'info' => serialize($info),
        'code' => $uniqid,
        'is_active' => 1,
      ];
      SendinblueManager::addSubscriberTable($data);
    }
    else {
      $uniqid = $data['code'] ?? '';
    }

    // Send confirm email.
    if ($uniqid&&$email_confirmation == '1') {
      SendinblueManager::sendEmail('confirm', $email, $uniqid, $list_id, '-1', $templage_id);
    }

  }

}
